import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor(private http: HttpClient) {
  }

  getJson(url: string) {
    return this.http.get(url);
  }

  getData() {
    return this.getJson('https://api.sandbox.voice123.com/providers/search/?service=voice_over').pipe(
      tap( (res: any) => {console.log(res); }),
      map( (res: any) => {
        return res.providers;
    })
    );
  }
}
