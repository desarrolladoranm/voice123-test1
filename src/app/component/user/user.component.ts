import { Component, OnInit } from '@angular/core';
import { JsonService} from '../services/json.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  busquedaRealizada: any[] = [];
  providers: any[] = [];
  result = [];
  mensaje: string;
  flag: boolean;

  constructor( public jsonService: JsonService) {

    this.flag = false;

    this.jsonService.getData().subscribe((res: any) => {
      this.result = res;
      this.providers = this.result;
      console.log(res);
    });
  }

  ngOnInit() {
  }

  search( buscar: string) {
     this.providers = this.result;
     this.busquedaRealizada = [];
     this.providers.forEach( data => {
       const ddd: string = data.user.name.toLowerCase();
       if ( ddd.search(buscar.toLowerCase()) >= 0) {
         this.busquedaRealizada.push(data);
       } else {
         this.flag = true;
         this.mensaje = 'No hay información referente a ese nombre';
       }
     });
     this.providers = this.busquedaRealizada;
  }

  reload() {
    this.providers = this.result;
  }
}
