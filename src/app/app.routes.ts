import { RouterModule , Routes } from '@angular/router';


import { UserComponent } from './component/user/user.component';


const APP_ROUTES: Routes = [

  {path: 'username', component: UserComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'Home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
